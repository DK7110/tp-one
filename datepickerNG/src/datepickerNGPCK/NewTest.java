package datepickerNGPCK;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class NewTest {
	public WebDriver driver;

@BeforeTest
public void beforeTest() throws InterruptedException {
	 System.setProperty("webdriver.chrome.driver","C:\\Users\\DK\\Desktop\\selenium webdriver\\chromeDriver.exe");
	 driver = new ChromeDriver();
	        driver.manage().window().maximize();
	        driver.manage().window().maximize();
	        driver.get("http://demo.guru99.com/test/");
	        
	        WebElement datebox= driver.findElement(By.xpath("/html/body/form/input[1]"));
			
			// fill the date as dd/mm/yyyy as 18/07/1997
			
			datebox.sendKeys("18071997");
			
			//Press tab to shift focus to time field
			
			datebox.sendKeys(Keys.TAB);
			
			// fill the time 11:00
			
			datebox.sendKeys("1100AM");
			
			driver.findElement(By.xpath("/html/body/form/input[2]")).click();
			Thread.sleep(5000);
}
  @Test
  public void Check_validation() {
	  
	  String actual_msg=driver.findElement(By.xpath("/html/body/div[2]")).getText();
		String expected_msg="Your Birth Date is 1997-07-18\n"
				+ "Your Birth Time is 11:00";
		Assert.assertEquals(actual_msg, expected_msg);
		
  
  }
  @AfterTest
  public void afterTest() {
  }

}
